//
//  PBWebViewController.m
//  Pinbrowser
//
//  Created by Mikael Konutgan on 11/02/2013.
//  Copyright (c) 2013 Mikael Konutgan. All rights reserved.
//

#import "PBWebViewController.h"
#import "Utils.h"

@interface PBWebViewController () <UIPopoverControllerDelegate>

@property (strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) UIBarButtonItem *stopLoadingButton;
@property (strong, nonatomic) UIBarButtonItem *reloadButton;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (strong, nonatomic) UIBarButtonItem *forwardButton;

@property (strong, nonatomic) UIPopoverController *activitiyPopoverController;

@property (strong, nonatomic) Utils *util;

@property (assign, nonatomic) BOOL toolbarPreviouslyHidden;

@end

@implementation PBWebViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _showsNavigationToolbar = YES;
}

- (void)load
{
    NSURLRequest *request = [NSURLRequest requestWithURL:self.URL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:4.0];
    [self.webView loadRequest:request];
    
    if (self.navigationController.toolbarHidden) {
        self.toolbarPreviouslyHidden = YES;
        if (self.showsNavigationToolbar) {
            [self.navigationController setToolbarHidden:NO animated:YES];
            self.navigationController.toolbar.barTintColor = [UIColor colorWithRed:33/255 green:33/255 blue:33/255 alpha:1];
            self.navigationController.toolbar.translucent = YES;
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:117/255.0f green:4/255.0f blue:32/255.0f alpha:1];
        }
    }
    _util = [[Utils alloc]init];
    [_util showLoader:self.view];
    self.backButton.enabled = NO;
    self.forwardButton.enabled = NO;
}

- (void)clear
{
    [self.webView loadHTMLString:@"" baseURL:nil];
    self.title = @"";
}

#pragma mark - View controller lifecycle

- (void)loadView
{
    [super loadView];
    self.webView = [[UIWebView alloc] init];
    self.webView.scalesPageToFit = YES;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationPortrait)
    {
        self.webView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
    }
    else
    {
        self.webView.frame = CGRectMake(0, 00, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    [self.view addSubview:self.webView];
    

    //self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    
    self.bannerView = [[GADBannerView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 90, self.view.frame.size.width, 50)];
    
    self.bannerView.adUnitID = @"ca-app-pub-0852828687090127/6643146362";
    self.bannerView.rootViewController = (id)self;
    self.bannerView.delegate = (id<GADBannerViewDelegate>)self;
    
    
    //self.bannerView.frame = CGRectMake(0, 0, 320, 50);
    
    [self.bannerView loadRequest:[GADRequest request]];
    
    [self.view addSubview:self.bannerView];
    //self.view = self.webView;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // do something before rotation
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation
{
    if(interfaceOrientation == UIInterfaceOrientationPortrait){
        NSLog(@"portrait");
    } else if(interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"LandscapeRight");
    } else if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        NSLog(@"LandscapeLeft");
    }
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    NSLog(@"YES");
    if(fromInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.bannerView.frame = CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 50);
    }
    else if(fromInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || fromInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if(orientation == UIInterfaceOrientationPortrait)
        {
            self.webView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
            self.bannerView.frame = CGRectMake(0, self.view.frame.size.height - 90, self.view.frame.size.width, 50);
        }
        else
        {
            self.webView.frame = CGRectMake(0, 00, self.view.frame.size.width, self.view.frame.size.height);
            self.bannerView.frame = CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 50);
        }
        NSLog(@"%f",self.view.frame.size.height);
    }
    else if(fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.bannerView.frame = CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 50);
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupToolBarItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.webView.delegate = self;
    if (self.URL) {
        [self load];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webView stopLoading];
    self.webView.delegate = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (self.toolbarPreviouslyHidden && self.showsNavigationToolbar) {
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

#pragma mark - Helpers

- (UIImage *)backButtonImage
{
    static UIImage *image;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        CGSize size = CGSizeMake(12.0, 21.0);
        UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        path.lineWidth = 1.5;
        path.lineCapStyle = kCGLineCapButt;
        path.lineJoinStyle = kCGLineJoinMiter;
        [path moveToPoint:CGPointMake(11.0, 1.0)];
        [path addLineToPoint:CGPointMake(1.0, 11.0)];
        [path addLineToPoint:CGPointMake(11.0, 20.0)];
        [path stroke];
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    });
    
    return image;
}

- (UIImage *)forwardButtonImage
{
    static UIImage *image;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        UIImage *backButtonImage = [self backButtonImage];
        
        CGSize size = backButtonImage.size;
        UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGFloat x_mid = size.width / 2.0;
        CGFloat y_mid = size.height / 2.0;
        
        CGContextTranslateCTM(context, x_mid, y_mid);
        CGContextRotateCTM(context, M_PI);
        
        [backButtonImage drawAtPoint:CGPointMake(-x_mid, -y_mid)];
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    });
    
    return image;
}

- (void)setupToolBarItems
{
//    self.stopLoadingButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
//                                                                           target:self.webView
//                                                                           action:@selector(stopLoading)];
//    
//    self.reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
//                                                                      target:self.webView
//                                                                      action:@selector(reload)];
    
    
    self.backButton = [[UIBarButtonItem alloc] initWithImage:[self backButtonImage]
                                                       style:UIBarButtonItemStylePlain
                                                      target:self.webView
                                                      action:@selector(goBack)];
    
    self.forwardButton = [[UIBarButtonItem alloc] initWithImage:[self forwardButtonImage]
                                                          style:UIBarButtonItemStylePlain
                                                         target:self.webView
                                                         action:@selector(goForward)];
    
    self.backButton.enabled = NO;
    self.forwardButton.enabled = NO;
    
//    UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
//                                                                                  target:self
//                                                                                  action:@selector(action:)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil
                                                                           action:nil];
    
    UIBarButtonItem *space_ = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                            target:nil
                                                                            action:nil];
    space_.width = 60.0f;
    
    self.toolbarItems = @[ space, self.backButton, space_, self.forwardButton, space];
}

- (void)toggleState
{
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
    
    NSMutableArray *toolbarItems = [self.toolbarItems mutableCopy];
    if (self.webView.loading) {
        toolbarItems[0] = self.stopLoadingButton;
    } else {
        toolbarItems[0] = self.reloadButton;
    }
    self.toolbarItems = [toolbarItems copy];
}

- (void)finishLoad
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
     [_util hideLoader];
    [self toggleState];
    
   
}

#pragma mark - Button actions

- (void)action:(id)sender
{
    if (self.activitiyPopoverController.popoverVisible) {
        [self.activitiyPopoverController dismissPopoverAnimated:YES];
        return;
    }
    
    NSArray *activityItems = @[self.URL];
    
    if (self.activityItems) {
        activityItems = self.activityItems;
    }
    
    UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                     applicationActivities:self.applicationActivities];
    vc.excludedActivityTypes = self.excludedActivityTypes;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:vc animated:YES completion:NULL];
    } else {
        if (!self.activitiyPopoverController) {
            self.activitiyPopoverController = [[UIPopoverController alloc] initWithContentViewController:vc];
        }
        
        self.activitiyPopoverController.delegate = self;
        
        UIBarButtonItem *barButtonItem = [self.toolbarItems lastObject];
        [self.activitiyPopoverController presentPopoverFromBarButtonItem:barButtonItem
                                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                                animated:YES];
    }
}

#pragma mark - Web view delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self toggleState];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self finishLoad];
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.URL = self.webView.request.URL;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self finishLoad];
}

#pragma mark - Popover controller delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.activitiyPopoverController = nil;
}

@end
