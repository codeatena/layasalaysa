
#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"


@interface Utils : NSObject<MBProgressHUDDelegate>{
    MBProgressHUD *progressHud;

}

- (void)showLoader:(UIView *)view;
- (void)showLoader:(UIView *)view  withMessage:(NSString *)message;
- (void)hideLoader;

@end