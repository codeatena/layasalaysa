
#import "Utils.h"
//#import "UIDevice+IdentifierAddition.h"
//#import "DataBaseManager.h"

@implementation Utils

- (void)showLoader:(UIView *)view {
    
    progressHud = [[MBProgressHUD alloc] initWithView:view];
	[view addSubview:progressHud];
	progressHud.delegate = self;
	progressHud.labelText = @"Please wait...";
	[progressHud show:YES];
}

- (void)showLoader:(UIView *)view  withMessage:(NSString *)message{
    progressHud = [[MBProgressHUD alloc] initWithView:view];
	[view addSubview:progressHud];
	progressHud.delegate = self;
	progressHud.labelText = message;
	[progressHud show:YES];
}

- (void)hideLoader{
    if([progressHud isHidden]==NO)
        [progressHud hide:YES];
}




/////////////////////////////////


@end
